# Prise connectée Tinternet - Fablab

Ce dépôt met à disposition les sources de la prise connectée qui a été offerte aux participants de la journée "Comprendre les enjeux de l'IoT(les objets connectés) et en fabriquer !" organisée conjointement par l'association Tinternet et cie et le Fablab des fabriques le 21 janvier 2023.

## Code arduino

Dans le répertoire Code Arduino se trouvent 2 version :
 - le projet tel qu'il a été remis aux participants : IOT_Tinternet_Fablab
 - une version modifiée pour les personnes n'ayant pas de connexion wifi domestique : IOT_Tinternet_Fablab_APMODE_only

## PCB

Le répertoire PCB contient les fichiers de conception électronique qui ont permis la réalisation du circuit imprimé, au format Kicad

## Boite

Le répertoire Boite contient le fichier SVG qui a permis la découpe de la boite au laser dans du medium de 3mm d'épaisseur

## Utilisation
Le principe et manuel d'utilisation sont décrits sur https://wiki-fablab.grandbesancon.fr/doku.php?id=projets:priseconnectee
