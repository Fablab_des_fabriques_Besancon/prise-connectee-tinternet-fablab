/*********
  Ce code est largement inspiré du code source de Rui Santos à travers 2 publications web :

  https://RandomNerdTutorials.com/esp32-wi-fi-manager-asyncwebserver/
  https://randomnerdtutorials.com/esp32-relay-module-ac-web-server/

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*********/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncTCP.h>
#include "LittleFS.h"
#include <ESP8266mDNS.h>
#include <WiFiClient.h>



// On crée un AsyncWebServer sur le port 80
AsyncWebServer server(80);

// Les paramètres dans nos requêtes HTTP POST
const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
const char* PARAM_INPUT_3 = "hostname";
const char* PARAM_INPUT_10 = "relay";
const char* PARAM_INPUT_11 = "state";

//Variables de stockage des valeurs insérées depuis le formulaire HTML
String ssid;
String pass;
String hostname;

bool configwifi;

int buttonState = 0; 

// Chemin des fichiers de stockage des infos WIFI
const char* ssidPath = "/ssid.txt";
const char* passPath = "/pass.txt";
const char* hostnamePath = "/hostname.txt";

// Variables de temporisation
unsigned long previousMillis = 0;
const long interval = 10000;  // intervalle laissé pour la connexion wifi

// GPIO du bouton
const int buttonPin = 4;

// GPIO de la LED
const int ledPin = 0;

// Variables will change:
int ledState = LOW;  // ledState used to set the LED

// Variables de temporisation LED
unsigned long previousledMillis = 0;
const long intervaled = 150;  // intervalle clignotement led

// On choisit le nombre et le type de relais
#define NUM_RELAYS  1
#define RELAY_NO false

// On assigne les GPIO aux relais
int relayGPIOs[NUM_RELAYS] = {5};


// Initialisation LittleFS
void initFS() {
  if (!LittleFS.begin()) {
    Serial.println("Erreur au montage SPIFFS");
  }
  Serial.println("SPIFFS monte correctement");
}



// Lecture des fichiers depuis LittleFS
String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path, "r");
  if (!file || file.isDirectory()){
    Serial.println("- failed to open file for reading");
    return String();
  }

  String fileContent;
  while (file.available()) {
    yield();
    fileContent = file.readStringUntil('\n');
    break;
  }
  file.close();
  return fileContent;
}



// Ecriture des fichiers sur SPIFFS
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\r\n", path);

  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("- failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("- file written");
  } else {
    Serial.println("- frite failed");
  }
}



// Vérification WiFi
void verifwifi() {
  buttonState = digitalRead(buttonPin);
  if ((ssid == "") || (buttonState == LOW)) {
    Serial.println("WIFI non paramétré");
    deletewificred();
    configwifi = false;
  }
  else {
    configwifi = true;
  }
}



// Suppression des fichiers de connexion WIFI
void deletewificred() {
  LittleFS.remove(ssidPath);
  LittleFS.remove(passPath);
  LittleFS.remove(hostnamePath);
  Serial.println("Fichiers de configuration wifi supprimés");
}



// Initialisation de la connexion WiFi
void connectWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(hostname.c_str()); //define hostname
  //WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
  WiFi.begin(ssid.c_str(), pass.c_str());
  Serial.println("Connexion WiFi...");

  unsigned long currentMillis = millis();
  previousMillis = currentMillis;

  while (WiFi.status() != WL_CONNECTED) {
    yield();
    currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
      Serial.println("Impossible de se connecter. Retour au mode point d'accès");
      deletewificred();
      ESP.restart();
    }
  }
  if (!MDNS.begin(hostname.c_str())) {
    Serial.println("Erreur mDNS");
  }
    // Add service to MDNS-SD
  MDNS.addService("http", "tcp", 80);
  Serial.println("Connexion WIFI : OK");
  Serial.print("Adresse IP de l'ESP8266 : ");
  Serial.println(WiFi.localIP());
  Serial.println("Accès depuis un navigateur à l'adresse http://" + hostname + ".local");
}



// Page html de contrôle du relai
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px} 
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 34px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 68px}
    input:checked+.slider {background-color: #2196F3}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}
  </style>
</head>
<body>
  <h2>Contrôle du relais</h2>
  %BUTTONPLACEHOLDER%
<script>function toggleCheckbox(element) {
  var xhr = new XMLHttpRequest();
  if(element.checked){ xhr.open("GET", "/update?relay="+element.id+"&state=1", true); }
  else { xhr.open("GET", "/update?relay="+element.id+"&state=0", true); }
  xhr.send();
}</script>
</body>
</html>
)rawliteral";



// Replaces placeholder with button section in your web page
String processor(const String& var){
  //Serial.println(var);
  if(var == "BUTTONPLACEHOLDER"){
    String buttons ="";
    for(int i=1; i<=NUM_RELAYS; i++){
      String relayStateValue = relayState(i);
      //buttons+= "<h4>Relay #" + String(i) + " - GPIO " + relayGPIOs[i-1] + "</h4><label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"" + String(i) + "\" "+ relayStateValue +"><span class=\"slider\"></span></label>";
      buttons+= "<label class=\"switch\"><input type=\"checkbox\" onchange=\"toggleCheckbox(this)\" id=\"" + String(i) + "\" "+ relayStateValue +"><span class=\"slider\"></span></label>";

    }
    return buttons;
  }
  return String();
}



String relayState(int numRelay){
  if(RELAY_NO){
    if(digitalRead(relayGPIOs[numRelay-1])){
      return "";
    }
    else {
      return "checked";
    }
  }
  else {
    if(digitalRead(relayGPIOs[numRelay-1])){
      return "checked";
    }
    else {
      return "";
    }
  }
  return "";
}



void setup() {
  
  Serial.begin(115200);
  
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);

  digitalWrite(ledPin, LOW);

  // On eteind les relais au démarrage
  for(int i=1; i<=NUM_RELAYS; i++){
    pinMode(relayGPIOs[i-1], OUTPUT);
    if(RELAY_NO){
      digitalWrite(relayGPIOs[i-1], HIGH);
    }
    else{
      digitalWrite(relayGPIOs[i-1], LOW);
    }
  }
  
  initFS();

  // On charge les données SPIFFS
  ssid = readFile(LittleFS, ssidPath);
  pass = readFile(LittleFS, passPath);
  hostname = readFile(LittleFS, hostnamePath);

  // On vérife si la connexion WIFI est déjà configurée
  verifwifi();
  if (configwifi == true) {
    Serial.println("Connexion WIFI configurée, on se connecte");
    connectWiFi();
  
    // On lance le serveur web
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send_P(200, "text/html", index_html, processor);
    });

  // Send a GET request to <ESP_IP>/update?relay=<inputMessage>&state=<inputMessage2>
  server.on("/update", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    String inputParam;
    String inputMessage2;
    String inputParam2;
    // GET input1 value on <ESP_IP>/update?relay=<inputMessage>
    if (request->hasParam(PARAM_INPUT_10) & request->hasParam(PARAM_INPUT_11)) {
      inputMessage = request->getParam(PARAM_INPUT_10)->value();
      inputParam = PARAM_INPUT_1;
      inputMessage2 = request->getParam(PARAM_INPUT_11)->value();
      inputParam2 = PARAM_INPUT_2;
      if(RELAY_NO){
        Serial.print("NO ");
        digitalWrite(relayGPIOs[inputMessage.toInt()-1], !inputMessage2.toInt());
      }
      else{
        Serial.print("NC ");
        digitalWrite(relayGPIOs[inputMessage.toInt()-1], inputMessage2.toInt());
      }
    }
    else {
      inputMessage = "No message sent";
      inputParam = "none";
    }
    Serial.println(inputMessage + inputMessage2);

    if (inputMessage2 == "1") {
      digitalWrite(ledPin, HIGH);
    }
    else {
      digitalWrite(ledPin, LOW);
    }
    request->send(200, "text/plain", "OK");
  });
  // Démarrage du serveur
  server.begin();
}
  
  else {
    // On crée un point d'accès wifi
    Serial.println("Setting AP (Access Point)");
    // NULL sets an open Access Point
    WiFi.softAP("ESP-WIFI-MANAGER", NULL);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("IP du point d'acces : ");
    Serial.println(IP);

    // Page d'accueil
    server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
      request->send(LittleFS, "/wifimanager.html", "text/html");
    });

    server.serveStatic("/", LittleFS, "/");

    server.on("/", HTTP_POST, [](AsyncWebServerRequest * request) {
      int params = request->params();
      for (int i = 0; i < params; i++) {
        AsyncWebParameter* p = request->getParam(i);
        if (p->isPost()) {
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_1) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            writeFile(LittleFS, ssidPath, ssid.c_str());
          }
          // HTTP POST pass value
          if (p->name() == PARAM_INPUT_2) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save value
            writeFile(LittleFS, passPath, pass.c_str());
          }

          // HTTP POST hostname value
          if (p->name() == PARAM_INPUT_3) {
            hostname = p->value().c_str();
            Serial.print("DNS Name set to: ");
            Serial.println(hostname);
            // Write file to save value
            writeFile(LittleFS, hostnamePath, hostname.c_str());
          }
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      request->send(200, "text/plain", "La configuration est terminée. L'ESP8266 va redémarrer, vous pourrez vous y connecter depuis l'url: http://" + hostname + ".local");
      delay(3000);
      ESP.restart();
    });
    server.begin();
     
  int i = 0;
  while (i == 0 ){
    yield();
    unsigned long currentledMillis = millis();
    if (currentledMillis - previousledMillis >= intervaled) {
      // save the last time you blinked the LED
      previousledMillis = currentledMillis;

      // if the LED is off turn it on and vice-versa:
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }

      // set the LED with the ledState of the variable:
      digitalWrite(ledPin, ledState);
    } 
  }
 }
}

void loop() {
MDNS.update();
}
